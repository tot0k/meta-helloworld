DESCRIPTION = "Recipe to build the hello world binary"

LICENSE="CLOSED"

# Add folder ${PN} to be able to add files from it
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

# Add file helloworld.c
SRC_URI = "file://helloworld.c"

# Set the build directory to the working directory.
# When using a git repo in SRC_URI, put this value to "${WORKDIR}/git"
S = "${WORKDIR}"

# This recipe provides package helloworld
PROVIDES += "helloworld"

do_compile() {
    ${CC} ${S}/helloworld.c ${LDFLAGS} -o helloworld
}

do_install() {
    install -d ${D}${bindir}
    install -m 0755 helloworld ${D}${bindir}
}
